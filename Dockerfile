FROM node:15

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
ENV PORT 5000

EXPOSE $PORT

COPY package.*json /usr/src/app/
RUN npm install

COPY . /usr/src/app

RUN npm run build

CMD [ "npm", "start" ]
