import * as Vue from 'vue'
import App from './components/App'

Vue.createApp(App).mount('#app')
