module.exports = {
    testEnvironment: 'node',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node', 'vue'],
    roots: ['<rootDir>'],
    testPathIgnorePatterns: ['/node_modules/'],
    verbose: true,
}
