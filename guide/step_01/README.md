# Step 1 - Setup Your Project and CI/CD

Before you start, here's where to make the separate work tab/window if you haven't already done so. Go ahead and duplicate this tab/window.
Designate one you will keep these instructions in, and one you will do all the clicking around and editing in. . . . Ok? Good, let's move on.

## Copy the project URL

This is the Demo App you will work on. Be sure to keep the URL (circled below) handy.

![create project](images/004-copy-project-link.png)

## Migrate Repo from GitHub to GitLab

Now that you are logged into your workshop account, let's start by migrating a project from GitHub (just to see how easy it is to do).

Click on the `New project` button in the upper right side:

![create project](images/001-create-new-project-001.png)

Click on `Import Project` tab:

![import project](images/002-import%20project.png)

You can migrate your repositories from GitHub to GitLab using the **GitHub importer**, which will migrate the whole project including the repository, its branches, commit history, issues, pull requests, pages, labels, milestones, etc. See the [GitHub Importer](https://docs.gitlab.com/ee/user/project/import/github.html) page for more details.

You can also just import any git repository by URL by using the **Repo by URL importer**. This imports the repository and its change history, branches, tags, etc. This is the method you'll use for this lab.

Enter the following URL into the `Git Repository URL` text field:

```
https://gitlab.com/agulina/express-vue-demo-app.git
```

Ensure the `Project slug` text field reads `express-vue-demo-app`.
Also ensure the `Visibility Level` of the repo is set to `Public`. Finally,
click the green `Create Project` button to start the migration process.

![import project](images/003-repo-by-url.png)

## Review CI/CD Configuration (Auto DevOps Pipeline)

GitLab provides an out-of-the-box end-to-end automatic CI/CD pipeline called Auto DevOps. Auto DevOps will build,
test, secure, deploy, and monitor your apps for you. This is enabled by default and set to deploy to production at the
end of the pipeline.

![import project](images/005-auto-devops.png)


Let's go take a quick look at that configuration.
Start by making sure that you are now in the `express-vue-demo-app` project that we just imported (look in the top left
corner to see which project name is showing).

Now, move your pointer over the `Settings` menu item on the bottom of the left control bar to get the `Settings` menu
to come up (but don't click it). Then slide over to click on the `CI/CD` menu option.

Next, click the `Expand` button on the `Auto DevOps` section:

![Auto DevOps section](images/006-auto-devops.png)

Here we can see how AutoDevOps is configured by default. Make sure that `Default to Auto DevOps pipeline` is checked.

Notice that the default **Deployment strategy** is to do `Continuous deployment to production`. Let's imagine that your organization is not ready for that just yet. Change the deployment strategy so that `Automatic deployment to staging, manual deployment to production` is selected.

You will note that by default `Continuous deployment to production` is selected, but that you can also select to have GitLab do `Continuous deployment to production using timed incremental rollout` or `Automatic deployment to staging, manual deployment to production` (which will also do manual incremental rollout).

Leave the default and click on the green `Save changes` button.

That's it! GitLab has probably already kicked off a pipeline to build, test, secure, deploy, and monitor your app for you. Let's go take a look under pipelines to confirm.

Mouse over the `CI/CD` menu item on the left control bar (but don't click it). Then slide over to click on the `Pipelines` menu option.

Here you can see that GitLab has kicked off the first pipeline on the project code. It will now do this automatically for all your commits to this project.

![Go to Pipelines screen](images/007-pipeline.png)

## Congratulations!!

![Celebration Tanuki](../images/shared/celebrate-tanuki.png)

<p>
You have set up your project by importing it from another repo, you've checked that your CI/CD pipeline is ready to build, test, secure, package, and deliuver your changes with every commit. Way to go. You are ready to focus on developing and delivering!

## Proceed to Step 2 - Capture and Track Your Work

Continue to [Step 2](../step_02/README.md)
