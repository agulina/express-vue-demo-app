# Step 2 - Capture and Track Your Work

So we now have a project all set to be worked on, but what is it we need to do on it? And there's probably more than one thing to do, so how do we keep track of all the things to fix or add? And how do we prioritize all those work items to be done?

GitLab has a solution for this too! GitLab has **Issues**, which represent work to be done, or problems to be solved, or work to be tracked, organized, and prioritized. Gitlab also has **Issue Boards** which are a very flexible and powerful way to sort your work items in many different ways (eg. SCRUM/Kanban boards, team capacity views, sprint planning, backlog management, etc)

So, let's switch gears for a minute and look at how GitLab's Issues and Issue Boards can help our new project get organized and stay on top of management and prioritization.

## Capture the Work to Be Done

Sometimes the path of innovation goes straight from brain to hands because ideas excite and inspire us so we take action. And that's fine. But many other times, capturing the work to be done can help us get a handle on what is the best work to do first.

Capturing the ideas as we think through a problem statement is a great way to stay focused yet still keep the bigger context in mind.

Even more important, it enables others to collaborate and contribute to the ideas.

Let's create an Issue for the change we'd like to make to our new project. Go to the top right side and you should see a `+` drop down. Pull it down and select `New issue` from the menu:

![issue_new](images/capture/issue_new.png)

In the resulting screen, add a `Title` and `Description` for your new issue:

![issue_info](images/step-002-001.png)

Scroll down and assign the issue to yourself using the provided link. You can see that there are lots of other options available to us, such as setting a due date, weight, milestone, applying labels, etc. For now we'll keep it simple. So, just click on the green **Submit issue** button:

![issue_submit](images/capture/issue_submit.png)

The new Issue will get created and GitLab will show it to us:

![issue_created](images/step-002-006.png)

## Planning and Prioritizing

Issue Boards help teams to track their work in many different ways, including SCRUM/Kanban, team load, sprint planning, backlog grooming, etc.

Let's set up an Issue Board for our new project (you can have multiple boards per project).

First let's set up some labels by clicking on **Labels** on the left panel under the Issues topic:

![boards_goto](images/step-002-002.png)

From there, click on `Generate a default set of labels`. This will help keep the work organised. Remember that you can always add more labels or customise them.

![boards_default_lists](images/step-002-003.png)

Next, visit the Board by clicking on the Board item under Issues.

![boards_goto](images/step-002-board.png)

![boards_default_lists](images/step-002-004.png)

From here we can see the Issue we created, and we have lists to sho them. To use the labels we have previously created, click on
`Edit List` and select a label. You'll see the new column appearing.

![boards_lists](images/step-002-list.png)

So imagine that on our Issue Boards we have backlog lists full of Issues. We want to groom it so we move one
to the **Suggestion** list which automatically adjusts its label, so that everyone else know what kind of Issue this is.

Issues in boards can be dragged and dropped around to organize work.

![boards_move_issue](images/step-002-005.png)

## Ready To Work? Start a Merge Request!

When you are ready to start work on an Issue you can open it up and get the details of what problem
to solve and see the discussions taking place about it. Click on the name of the Issue to open it:

The Issue will open and you can edit it. [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) recommends starting a Merge Request as soon as you are ready to start working on a solution. GitLab makes that easy to do right from the Issue.

Click on the green **Create merge request** button:

![issue_created](images/step-002-006-mr.png)

GitLab will automatically create a Merge Request to work on the solution to this Issue, it will create a feature branch for the work to be done on, it will associate the MR to the Issue so once the work is pushed to production the issue gets closed and the resources freed.

From here you can now start editing files in the new branch. Click on the **Open in Web IDE** button in the middle upper half of the MR:

![issue_created](images/step-002-007.png)
## Congratulations!!

![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)

<p>
In this lab you have lightly touched GitLab's issue tracking and planning capabilities. You saw the recommended flow for starting to work on something (of course, GitLab is flexible so it'll support however you choose to work). From this point forward any commits to the new branch will be tracked by the Merge Request you just created, and GitLab will maintain traceability from the Issue all the way through to the production deployments.

## Proceed to Step 3 - Make Some Code Changes

Continue to [Step 3](../step_03/README.md)
