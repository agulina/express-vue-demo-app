# Step 3 - Make Some Code Changes

In the last lab we got setup to work and then opened the built-in Web IDE on our new branch.
You can of course edit with any other git compatible tooling, but we'll keep it simple for today.
Let's now go and make a small change to our project.

## Edit the Code
In the Web IDE, on the left side file tree, navigate down to `App.vue` and click on it to view and edit
(`views` -> `components`-> `App.vue`)

![Navigate to file](images/003-editor-002.png)

1. Remove the word `Demo` (line 5)
![Navigate to file](images/003-editor-002-line.png)

## Commit the Changes
When you are done editing, your file should look as pictured below. Click on the blue `Commit...`button
(Hint: You might have to do this twice depending on the size of your browser window).

![Commit changes](images/003-editor-003.png)

Add a short commit message and make sure the option of `Commit to <your branch name> branch` is selected,
and then click on the green `Stage & Commit` button.

![Commit changes](images/003-editor-004.png)


## Congratulations!!

![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)

<p>
You have made several code changes, and on committing them, GitLab automatically picked up and ran your changes through the pipeline, including building them (remember you never defined how to do that), testing them, security scanning them, packaging them, deploying them, and monitoring them.

## Proceed to Step 4 - Review Pipeline Results and Deploy to Production

Continue to [Step 4](../step_04/README.md)
