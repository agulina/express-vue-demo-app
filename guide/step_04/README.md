# Step 4 - Review Pipeline Results and Deploy to Production

GitLab automatically kicks off a pipeline to build your code changes, test them, and then deploy them to a personal review environment (called a Review App) where you and other stakeholders can validate your changes. Behind the scenes GitLab and Google Kubernetes Services are orchestrating to not only scale up the runners during the test stages, but also to spin up (and eventually down) the review environments.

## Look at the Running Pipeline

From the Web IDE in the last lab, the pipeline status is displayed as it runs. You can go see the pipeline graph which shows more details by clicking on either the `pie chart` or the `pipeline ID` on the bottom left of your browser window:

![MR with running pipeline](images/deliver/mr_running_pipeline.png)

Looking at the pipeline you can see all the tests (security and otherwise) that GitLab is running in parallel, as well as the review app setup and further testing. Remember, we didn't configure any pipelines and there was no pipeline defined as code in our repo. This is all out of the box behavior.

![pipeline graph](images/deliver/pipeline_graph.png)

The pipeline will take several minutes to complete. Once a job is running you can click into it to see live output and get more information about it. Go ahead and look into the details of a job.

![goto job details](images/deliver/goto_job_details.png)

Click on the `!1` link to bring you back to the merge request that this pipeline was launched from.

![job details](images/deliver/job_details.png)

## Review Your Changes

Once back at the Merge Request click on the `View app` button to take a look at the running code changes.

![MR ready to merge](images/deliver/mr_ready_to_merge.png)

Remember, this is your code change running from your branch. Nothing has been pushed to the main branch yet. Once you've confirmed your code changes resulted in the changes you wanted, you can close this window/tab and go back to the Merge Request.

![Review app](images/deliver/review_app.png)

### Merge the Changes and Send It to Production

Once back on the merge request, you can look at the other test results, such as the performance and code quality results, the security scan results, and the license management results, and decide if you want to merge the changes.

If you think the work is ready to merge, you still need to take the MR out of WIP status. This stands for **Work In Progress** and GitLab sets this on all new MR's automatically until they are considered done work.

Click on the **Resolve WIP status** button to remove the WIP flag:

![resolve_wip](images/deliver/resolve_wip.png)

Once the WIP flag is cleared the `Merge` button will become active. Click on the green `Merge` button:

![Merge it!](images/deliver/mr_merge_it.png)

GitLab will complete the merge, fire off another pipeline to deliver the changes to production, and then clean up the completed branches and Kubernetes resources.

## Congratulations!!

![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)
![Celebration Tanuki](../images/shared/celebrate-tanuki.png)

<p>
You reviewed the results of your changes and determined the code was fit for production. You then informed GitLab and GitLab took care of delivering it to staging for you. But remember, we set GitLab to require manual release to production, so from staging you released it and now the world can enjoy the new value you have brought them.

Great job!!!

## Next Steps/Learn More

We have only scratched the surface here, but this hopefully provides you at least a starting point to explore GitLab further and see what it can do for you.

### Explore More of GitLab

Sign up for a [free 30-day trial license](http://bit.ly/gitlab_free_trial) (SaaS offering included).

GitLab is a single application for the entire software development life cycle. It offers functionality from planning to creation, testing to packaging, deploying,
securing, and monitoring, allowing you to execute your entire workflow within
it's single interface. GitLab runs basically anywhere you want, from on-premise
bare metal all the way up to managed Kubernetes offerings.

### Resources

Gitlab  
* [Get a free 30 day GitLab Ultimate trial license](https://about.gitlab.com/free-trial/)  
* [GitLab Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)  
* [Docs on Customizing Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/customize.html)  
