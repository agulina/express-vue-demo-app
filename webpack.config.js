const path = require('path')
const webpack = require('webpack')
const HtmlPlugin = require('html-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')

const scriptsPath = path.resolve(__dirname, './')
const srcBasePath = `${scriptsPath}/views`
const destPath = `${scriptsPath}/public`

module.exports = {
    devServer: {
        contentBase: destPath,
        historyApiFallback: true,
        hot: true,
        inline: true,
        port: 9000,
        watchOptions: {
            ignored: /node_modules/,
        },
    },
    entry: {
        main: `${srcBasePath}/index.js`,
    },
    module: {
        rules: [
            {
                test: /\.vuex?$/,
                loader: 'vue-loader',
            },
            {
                test: /\.(s*)[a|c]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'vue-style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: ['file-loader'],
            },
        ],
    },
    output: {
        path: destPath,
        filename: '[name].[hash].js',
    },
    plugins: [
        new HtmlPlugin({
            template: `${srcBasePath}/index.html`,
        }),
        new VueLoaderPlugin(),
    ],
    resolve: {
        extensions: ['*', '.js', '.vue', '.json', '.mjs', '.css'],
        alias: {
            vue: '@vue/runtime-dom',
        },
    },
}
