### Node Express + Vue 3 Demo App

This project soon to be a template on GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/agulina/express-vue-demo-app.git).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.

### Developing locally

Start the Webpack [DevServer](https://webpack.js.org/configuration/dev-server/) by running `npm development` and have fun!

---

Project based on [express](https://gitlab.com/gitlab-org/project-templates/express) modified with love ❤️
