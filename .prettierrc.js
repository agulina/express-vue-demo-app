module.exports = {
    bracketSpacing: true,
    semi: false,
    singleQuote: true,
    trailingComma: 'es5',
    tabWidth: 4,
    vueIndentScriptAndStyle: true,
}
